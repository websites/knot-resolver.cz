:title: Knot Resolver 5.3.1 released
:slug: knot-resolver-5.3.1
:date: 2021-03-31 17:00
:lang: en

Improvements
------------
- policy.STUB: try to avoid TCP (compared to 5.3.0; !1155)
- validator: downgrade NSEC3 records with too many iterations (>150; !1160)
- additional improvements to nameserver selection algorithm (!1154, !1150)

Bugfixes
--------
- dnstap module: don't break request resolution on dnstap errors (!1147)
- cache garbage collector: fix crashes introduced in 5.3.0 (!1153)
- policy.TLS_FORWARD: better avoid dead addresses (#671, !1156)
