:title: Knot Resolver 5.5.2 released
:slug: knot-resolver-5.5.2
:date: 2022-08-16 14:30
:lang: en

Improvements
------------
- support libknot 3.2 (!1309)
- priming module: hide failures from the default log level (!1310)
- reduce memory usage in some cases (!1328)

Bugfixes
--------
- daemon/http: improve URI checks to fix some proxies (#746, !1311)
- daemon/tls: fix a double-free for some cases of policy.TLS_FORWARD (!1314)
- hints module: improve parsing comments in hosts files (!1315)
- renumber module: fix renumbering with name matching again (#760, !1334)
