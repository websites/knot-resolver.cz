Contact
#######

:slug: contact
:lang: en
:menu: Contact
:menuorder: 6


* For software support please see `support section </support/>`_.

Email
=====

* `knot-resolver@labs.nic.cz`_

.. _knot-resolver@labs.nic.cz: mailto:knot-resolver@labs.nic.cz


Reporting Security Issues
=========================

* When reporting sensitive security issues that require encryption, please use
  the following address and its associated PGP key.
* Our PGP keyring: https://secure.nic.cz/files/knot-resolver/kresd-keyblock.asc
* `vladimir.cunat@nic.cz`_

.. _vladimir.cunat@nic.cz: mailto:vladimir.cunat@nic.cz

Address
=======

* CZ.NIC, z. s. p. o.

  Milesovska 1136/5

  130 00 Praha 3

  Czech Republic
