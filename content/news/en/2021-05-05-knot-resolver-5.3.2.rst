:title: Knot Resolver 5.3.2 released
:slug: knot-resolver-5.3.2
:date: 2021-05-05 12:00
:lang: en

Security
--------
- validator: fix 5.3.1 regression on over-limit NSEC3 edge case (!1169)
  Assertion might be triggered by query/answer, potentially DoS.

Improvements
------------
- cache: improve handling write errors from LMDB (!1159)
- doh2: improve handling of stream errors (!1164)

Bugfixes
--------
- dnstap module: fix repeated configuration (!1168)
- validator: fix SERVFAIL for some rare dynamic proofs (!1166)
- fix SIGBUS on uncommon ARM machines (unaligned access; !1167, #426)
- cache: better resilience on abnormal termination/restarts (!1172)
- doh2: fix memleak on stream write failures (!1161)
