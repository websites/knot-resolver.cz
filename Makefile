PELICAN=pelican
PELICANOPTS=--fatal errors

DEFAULT_LANG=$(shell python -c "`grep "^DEFAULT_LANG" pelicanconf.py`; print(DEFAULT_LANG)")
SITEURL=$(shell python -c "`grep "^SITEURL" pelicanconf.py`; print(SITEURL.replace('/', '\/'))")

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/$(shell python -c "`grep "^PATH" pelicanconf.py`; print(PATH)")
OUTPUTDIR=$(BASEDIR)/$(shell python -c "`grep "^OUTPUT_PATH" pelicanconf.py`; print(OUTPUT_PATH)")
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

FTP_HOST=localhost
FTP_USER=anonymous
FTP_TARGET_DIR=/

SSH_HOST=localhost
SSH_PORT=22
SSH_USER=root
SSH_TARGET_DIR=/var/www

DROPBOX_DIR=~/Dropbox/Public/

all: build

help:
	@echo 'Makefile for a pelican Web site                                        '
	@echo '                                                                       '
	@echo 'Usage:                                                                 '
	@echo '   make html                        (re)generate the web site          '
	@echo '   make clean                       remove the generated files         '
	@echo '   make regenerate                  regenerate files upon modification '
	@echo '   make publish                     generate using production settings '
	@echo '   make serve                       serve site at http://localhost:8000'
	@echo '   make devserver                   start/restart develop_server.sh    '
	@echo '   ssh_upload                       upload the web site via SSH        '
	@echo '   rsync_upload                     upload the web site via rsync+ssh  '
	@echo '   dropbox_upload                   upload the web site via Dropbox    '
	@echo '   ftp_upload                       upload the web site via FTP        '
	@echo '   github                           upload the web site via gh-pages   '
	@echo '                                                                       '


build: html

html: clean $(OUTPUTDIR)/index.html
	sed -i "s,__SITEURL__,../..,g" $(OUTPUTDIR)/theme/css/*
	cd $(OUTPUTDIR) && ln -s 2018-04-23-knot-resolver-2.3.0.html 2018-04-20-knot-resolver-2.3.0.html
	@echo 'Done'

$(OUTPUTDIR):
	mkdir $(OUTPUTDIR)

$(OUTPUTDIR)/%.html:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

clean: $(OUTPUTDIR)
	find $(OUTPUTDIR) -mindepth 1 -delete

regenerate: clean
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
	cd $(OUTPUTDIR) && python -m http.server

devserver:
	$(BASEDIR)/develop_server.sh restart

publish:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)
	sed -i "s,__SITEURL__,../..,g" $(OUTPUTDIR)/theme/css/*
	cd $(OUTPUTDIR) && ln -s 2018-04-23-knot-resolver-2.3.0.html 2018-04-20-knot-resolver-2.3.0.html

ssh_upload: publish
	scp -P $(SSH_PORT) -r $(OUTPUTDIR)/* $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR)

rsync_upload: publish
	rsync -e "ssh -p $(SSH_PORT)" -P -rvz --delete $(OUTPUTDIR) $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR)

dropbox_upload: publish
	cp -r $(OUTPUTDIR)/* $(DROPBOX_DIR)

ftp_upload: publish
	lftp ftp://$(FTP_USER)@$(FTP_HOST) -e "mirror -R $(OUTPUTDIR) $(FTP_TARGET_DIR) ; quit"

github: publish
	ghp-import $(OUTPUTDIR)
	git push origin gh-pages

.PHONY: html help clean regenerate serve devserver publish ssh_upload rsync_upload dropbox_upload ftp_upload github
.NOTPARALLEL:
