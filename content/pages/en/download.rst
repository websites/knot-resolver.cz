:title: Download
:slug: download
:menu: Download
:lang: en
:menuorder: 1

We recommend using the latest Knot Resolver version. Our upstream releases
undergo extensive automated testing and are suitable for production.

Packages available in your distribution's may be outdated. Follow the
instructions below to obtain the latest Knot Resolver version for your
distribution.

Please note that we occasionally release a major version which may require
manual upgrade. Please subscribe to our `knot-resolver-announce
<https://lists.nic.cz/postorius/lists/knot-resolver-announce.lists.nic.cz/>`__ mailing list
to be notified of new releases, including upgrade instructions if necessary.


Debian / Ubuntu
---------------

Please use our `official repos <https://pkg.labs.nic.cz/doc/?project=knot-resolver>`__
for Debian and Ubuntu.
Debian unstable and testing usually contain latest Knot Resolver version.

After that ``apt`` will keep updating knot-resolver 5.x packages from our repositories.

If you used our older repo until now, you may want to also uninstall the helper package
by ``apt purge knot-resolver-release``.

Enterprise Linux 7, 8, 9
------------------------

Use Fedora EPEL.

::

   yum install -y epel-release
   yum install -y knot-resolver

Package updates are delayed by about one week after release. To obtain the
latest released version early, you can use the epel-testing repository.

::

   yum install -y --enablerepo epel-testing knot-resolver

Fedora
------

Use the distribution's repositories where we maintain up-to-date packages.

::

   dnf install -y knot-resolver

Package releases are delayed by about a week. To obtain the latest released
version early, you can use the updates-testing repository.

::

   dnf install -y --enablerepo updates-testing knot-resolver

openSUSE
--------

Just add our `COPR repository <https://copr.fedorainfracloud.org/coprs/g/cznic/knot-resolver5>`__,
based on the variant of your openSUSE:
::

  # Leap 15.5
  zypper addrepo https://copr.fedorainfracloud.org/coprs/g/cznic/knot-resolver5/repo/opensuse-leap-15.5/group_cznic-knot-resolver5-opensuse-leap-15.5.repo

  # Tumbleweed
  zypper addrepo https://copr.fedorainfracloud.org/coprs/g/cznic/knot-resolver5/repo/opensuse-tumbleweed/group_cznic-knot-resolver5-opensuse-tumbleweed.repo

Then you can install as usual with
::

   zypper install knot-resolver

Arch Linux
----------

::

   pacman -S knot-resolver


Source code
===========

-  `Knot Resolver
   5.7.4 - stable <https://secure.nic.cz/files/knot-resolver/knot-resolver-5.7.4.tar.xz>`__
   (`asc <https://secure.nic.cz/files/knot-resolver/knot-resolver-5.7.4.tar.xz.asc>`__,
   `sha256 <https://secure.nic.cz/files/knot-resolver/knot-resolver-5.7.4.tar.xz.sha256>`__)
-  `Knot Resolver
   6.0.11 - early-access <https://secure.nic.cz/files/knot-resolver/knot-resolver-6.0.11.tar.xz>`__
   (`asc <https://secure.nic.cz/files/knot-resolver/knot-resolver-6.0.11.tar.xz.asc>`__,
   `sha256 <https://secure.nic.cz/files/knot-resolver/knot-resolver-6.0.11.tar.xz.sha256>`__)
-  `Old releases <https://secure.nic.cz/files/knot-resolver/?C=N;O=D>`__

New releases are signed with one of our developer PGP keys:

- https://secure.nic.cz/files/knot-resolver/kresd-keyblock.asc

::

  pub   rsa4096 2022-03-07 [SC] [expires: 2026-03-03]
        3057EE9A448F362D74205A779AB120DA0A76F6DE
  uid           Ales Mrazek <ales.mrazek@nic.cz>

  pub   rsa4096 2016-10-16 [SCA] [expires: 2027-11-11]
        B6006460B60A80E782062449E747DF1F9575A3AA
  uid           Vladimír Čunát (work) <vladimir.cunat@nic.cz>

Older releases may be signed using the following keys:

- `6DC2B0CB5935EA7A39614AA732B22D20C9B4E680.asc
  <https://keys.openpgp.org/vks/v1/by-fingerprint/6DC2B0CB5935EA7A39614AA732B22D20C9B4E680>`__
- `4A8BA48C2AED933BD495C509A1FBA5F7EF8C4869.asc
  <https://keys.openpgp.org/vks/v1/by-fingerprint/4A8BA48C2AED933BD495C509A1FBA5F7EF8C4869>`__
- `BE26EBB9CBE059B3910CA35BCE8DD6A1A50A21E4.asc
  <https://keys.openpgp.org/vks/v1/by-fingerprint/BE26EBB9CBE059B3910CA35BCE8DD6A1A50A21E4>`__
- `DEF35D16E5AE59D820BDF780ACE24DA9EE37A832.asc
  <https://keys.openpgp.org/vks/v1/by-fingerprint/DEF35D16E5AE59D820BDF780ACE24DA9EE37A832>`__

See the `Building from sources <https://knot-resolver.readthedocs.io/en/latest/build.html>`__ for instructions how to build from sources.

Docker image
============

This is simple way to experiment and does not require any dependencies or system
modifications.  The images are not designed for production use.  See the build page
`hub.docker.com/r/cznic/knot-resolver <https://hub.docker.com/r/cznic/knot-resolver>`__
for more information and options.

