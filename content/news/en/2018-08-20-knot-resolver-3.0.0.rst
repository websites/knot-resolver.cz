:title: Knot Resolver 3.0.0 released
:slug: knot-resolver-3.0.0
:date: 2018-08-20 11:00
:lang: en

Incompatible changes
--------------------
- cache: fail lua operations if cache isn't open yet (!639)
  By default cache is opened *after* reading the configuration,
  and older versions were silently ignoring cache operations.
  Valid configuration must open cache using `cache.open()` or `cache.size =`
  before executing cache operations like `cache.clear()`.
- libknot >= 2.7.1 is required, which brings also larger API changes
- in case you wrote custom Lua modules, please consult
  https://knot-resolver.readthedocs.io/en/latest/lib.html#incompatible-changes-since-3-0-0
- in case you wrote custom C modules, please see compile against
  Knot DNS 2.7 and adjust your module according to messages from C compiler
- DNS cookie module (RFC 7873) is not available in this release,
  it will be later reworked to reflect development in IEFT dnsop working group
- version module was permanently removed because it was not really used by users;
  if you want to receive notifications about new releases please subscribe to
  https://lists.nic.cz/postorius/lists/knot-resolver-announce.lists.nic.cz/

Bugfixes
--------
- fix multi-process race condition in trust anchor maintenance (!643)
- ta_sentinel: also consider static trust anchors not managed via RFC 5011

Improvements
------------
- reorder_RR() implementation is brought back
- bring in performance improvements provided by libknot 2.7
- cache.clear() has a new, more powerful API
- cache documentation was improved
- old name "Knot DNS Resolver" is replaced by unambiguous "Knot Resolver"
  to prevent confusion with "Knot DNS" authoritative server
