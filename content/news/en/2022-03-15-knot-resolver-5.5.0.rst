:title: Knot Resolver 5.5.0 released
:slug: knot-resolver-5.5.0
:date: 2022-03-15 14:30
:lang: en

Improvements
------------
- extended_errors: module for extended DNS error support, RFC8914 (!1234)
- policy: log policy actions; useful for RPZ debugging (!1239)
- policy: new action policy.IPTRACE for logging request origin (!1239)
- prefill module: prepare for ZONEMD, improve performance (!1225)
- validator: conditionally ignore SHA1 DS, as SHOULD by RFC4509 (!1251)
- lib/resolve: use EDNS padding for outgoing TLS queries (!1254)
- support for PROXYv2 protocol (!1238)
- lib/resolve, policy: new NO_ANSWER flag for not responding to clients (!1257)

Incompatible changes
--------------------
- libknot >= 3.0.2 is required

Bugfixes
--------
- doh2: fix CORS by adding `access-control-allow-origin: *` (!1246)
- net: fix listen by interface - add interface suffix to link-local IPv6 (!1253)
- daemon/tls: fix resumption for outgoing TLS (e.g. TLS_FORWARD) (!1261)
- nameserver selection: fix interaction of timeouts with reboots (#722, !1269)
