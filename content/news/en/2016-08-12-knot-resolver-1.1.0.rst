:title: Knot Resolver 1.1.0 released
:slug: knot-resolver-1.1.0
:date: 2016-08-12 08:00
:lang: en

Knot Resolver 1.1.0 has been released.

The second production-ready release of Knot Resolver has been released.

The list of notable features:

-  RFC7873 DNS Cookies
-  RFC7858 DNS over TLS
-  `HTTP/2 web interface, RESTful
   API <https://knot-resolver.readthedocs.io/en/v1.1.0/modules.html#http-2-services>`__
-  `Metrics exported in
   Prometheus <https://knot-resolver.readthedocs.io/en/v1.1.0/modules.html#enabling-prometheus-metrics-endpoint>`__
-  `DNS firewall
   module <https://knot-resolver.readthedocs.io/en/v1.1.0/modules.html#dns-application-firewall>`__
-  Explicit CNAME target fetching in strict mode
-  Query minimisation improvements
-  Improved integration with systemd

For the full list of features, please see the `online
documentation <https://knot-resolver.readthedocs.io/>`__.
