:title: Documentation
:slug: documentation
:menu: Documentation
:lang: en
:menuorder: 2

.. The inconsistency in links (sometimes absolute and sometimes relative) is
   here beceuase 'stable' is a symlink in this repo, while 'latest' is
   reverse-proxied to GitLab Pages by our frontend server, and thus is not
   available locally.

Quickstart
==========

1. `Install packages <stable/quickstart-install.html>`_
2. `Run daemon <stable/quickstart-startup.html>`_

Knot Resolver is now running on localhost and will recursively resolve any
incoming queries. Now you can query it or `play with its configuration <stable/quickstart-config.html>`_.

Comprehensive documentation
===========================

Docs for the latest stable version 5 are available online at
`knot-resolver.cz/documentation/stable/ <stable/>`__.

Docs for the latest early-access version 6.0.x are available online at
`knot-resolver.cz/documentation/latest/ <https://www.knot-resolver.cz/documentation/latest/>`__

Offline documentation is also available in *knot-resolver-doc* package for most
distributions.

.. *Read the Docs* also offers downloads of
   `PDF <https://readthedocs.org/projects/knot-resolver/downloads/pdf/stable/>`__
   and
   `EPUB <https://readthedocs.org/projects/knot-resolver/downloads/epub/stable/>`__
   formats for viewing offline.

Upgrading
=========
Occasionally a major version release might require a manual upgrade.
Please refer to
`Upgrading Guide <stable/upgrading.html>`_.

Versions
========

.. Please keep these lists sorted in descending order, i.e. newest version
   first. For now, 5.x is first, because it's stable, but once 6.1 is released,
   the plan is to put 6.x first.

5.x (stable)
------------

* `v5.7.4 <v5.7.4/>`_
* `v5.7.3 <v5.7.3/>`_
* `v5.7.2 <v5.7.2/>`_

6.x (early-access)
------------------

* `v6.0.11 <v6.0.11/>`_
* `v6.0.10 <v6.0.10/>`_
* `v6.0.9 <v6.0.9/>`_
* `v6.0.8 <v6.0.8/>`_
* `v6.0.7 <v6.0.7/>`_
* `v6.0.6 <v6.0.6/>`_
