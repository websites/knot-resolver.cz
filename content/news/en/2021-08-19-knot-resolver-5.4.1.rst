:title: Knot Resolver 5.4.1 released
:slug: knot-resolver-5.4.1
:date: 2021-08-19 15:00
:lang: en

Improvements
------------
- docker: base image on Debian 11 (!1203)

Bugfixes
--------
- fix build without doh2 support after 5.4.0 (!1197)
- fix policy.DEBUG* logging and -V/--version after 5.4.0 (!1199)
- doh2: ensure memory from unsent streams is freed (!1202)
