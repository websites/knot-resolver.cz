:title: Knot Resolver 1.3.1
:slug: knot-resolver-1.3.1
:date: 2017-06-23 14:00
:lang: en

Knot Resolver 1.3.1 has been released.

Bugfixes
========

-  modules/http: fix finding the static files (bug from 1.3.0)
-  policy.FORWARD: fix some cases of CNAMEs obstructing search for zone
   cuts
