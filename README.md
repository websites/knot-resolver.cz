```
virtualenv knot-resolver-web
cd knot-resolver-web
git clone git@gitlab.nic.cz:websites/knot-resolver.cz.git git
source bin/activate
cd git
pip install -r requirements.txt
git submodule init
git submodule update
make html
```
