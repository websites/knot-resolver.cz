:title: Knot Resolver 5.7.3 released
:slug: knot-resolver-5.7.3
:date: 2024-05-30 14:45
:lang: en

Improvements
------------
- stats: add separate metrics for IPv6 and IPv4 (!1544)

Bugfixes
--------
- fix NSEC3 records missing in answer for positive wildcard expansion
  with the NSEC3 having over-limit iteration count (#910, !1550)
