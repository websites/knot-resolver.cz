:title: Knot Resolver Beta 3 released
:slug: knot-resolver-beta3
:date: 2016-01-31 10:00
:lang: en

Knot Resolver 1.0.0-beta3 released.

-  Outbound query deduplication
-  CLI tools: kresd-host and kresd-query
-  Automatic
   `bootstrap <http://knot-resolver.readthedocs.org/en/v1.0.0/daemon.html#enabling-dnssec>`__
   of root TA
-  Built-in
   `hardening <http://knot-resolver.readthedocs.org/en/v1.0.0/build.html#building-with-security-compiler-flags>`__
-  More
   `metrics <http://knot-resolver.readthedocs.org/en/v1.0.0/modules.html#statistics-collector>`__
   (dropped, nodata, timeout)
-  Ported to libknot 2.1
-  Quiet and no-configuration mode (see kresd -h)
-  Negotiated socket buffer sizes
-  Various DNSSEC validator fixes
-  Fixed receiving very large TCP payloads
-  Support for PowerPC e500
