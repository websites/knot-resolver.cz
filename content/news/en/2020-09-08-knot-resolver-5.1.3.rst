:title: Knot Resolver 5.1.3 released
:slug: knot-resolver-5.1.3
:date: 2020-09-08 13:59
:lang: en

Improvements
------------
- capabilities are no longer constrained when running as root (!1012)
- cache: add percentage usage to cache.stats() (#580, !1025)
- cache: add number of cache entries to cache.stats() (#510, !1028)
- aarch64 support again, as some systems still didn't work (!1033)
- support building against Knot DNS 3.0 (!1053)

Bugfixes
--------
- tls: fix compilation to support net.tls_sticket_secret() (!1021)
- validator: ignore bogus RRSIGs present in insecure domains (!1022, #587)
- build if libsystemd version isn't detected as integer (#592, !1029)
- validator: more robust reaction on missing RRSIGs (#390, !1020)
- ta_update module: fix broken RFC5011 rollover (!1035)
- garbage collector: avoid keeping multiple copies of cache (!1042)
