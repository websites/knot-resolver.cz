:title: Knot Resolver 1.2.1 released
:slug: knot-resolver-1.2.1
:date: 2017-02-01 20:53
:lang: en

Knot Resolver 1.2.1 has been released.

It was discovered during internal testing that under special query
combinations the Knot Resolver can provide Insecure data from the cache
on negative answers that would be otherwise Bogus due invalid
signatures.
