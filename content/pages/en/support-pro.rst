:title: Professional support
:slug: support/pro
:lang: en
:menuorder: 0

CZ.NIC organization offers professional support for users who depend
on Knot Resolver. **If DNS resolving is critical for your bussiness
professional support contract is strongly recommended.**

Requests from customers always take priority over
`free support <https://www.knot-resolver.cz/support/free/>`_
and our support contracts provide customers with SLA and NDA they need.

Contact us
==========
Please contact us on e-mail
`knot-resolver@labs.nic.cz <mailto:knot-resolver@labs.nic.cz>`_,
we are happy to tailor support and consultancy contracts to your needs.

Our experience
==============
Knot Resolver team is part of `CZ.NIC organization <https://www.nic.cz/>`_
and has plenty of experience with solving wide range of issues:
Misconfigurations, convoluted interactions between load balancers
and authoritative servers, debugging crashes in DNS servers and clients,
and also finding security issues in DNS software.

CZ.NIC also offers services for
`authoritative server Knot DNS <https://www.knot-dns.cz/>`_,
`routing daemon BIRD <https://bird.network.cz/>`_,
and since 1998 we also operate our own anycast network for
DNS top-level domain CZ. This gives us insight and better understanding
of problems in real-world deployments.

Open-source funding
===================
Customers who purchase our professional services directly support
research and development of open-source DNS software. Thank you!
