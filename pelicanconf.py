#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals
import datetime
import re
import pyphen

SITENAME = "Knot Resolver"
SITESUBTITLE = "Resolve DNS names like it’s %d" % datetime.date.today().year
SITE_LOGO = "/images/logo.svg"
SITEURL = "https://www.knot-resolver.cz"
ROOTURL = "https://www.knot-resolver.cz"
NEWS_TITLE = "Releases"
DOWNLOAD_BUTTON = "Download"
DOWNLOAD_URL = "/download/"

TIMEZONE = "Europe/Prague"

PATH = "content"
OUTPUT_PATH = "public"

DEFAULT_LANG = "en"
LOCALE = "en_US.UTF-8"
DEFAULT_DATE_FORMAT = "%A, %B %-d, %Y"


FOOTER_LINKS = (
    ("Labs", "https://labs.nic.cz/"),
    ("FRED", "http://fred.nic.cz/"),
    ("BIRD", "http://bird.network.cz/"),
    ("Turris Omnia", "https://omnia.turris.cz/"),
    ("CSIRT", "https://www.csirt.cz/"),
    ("Turris", "https://www.turris.cz/"),
    ("Web scanner", "https://www.skenerwebu.cz/")
)


NON_BREAKABLE_WORDS = [
    "Knot",
]


def hyphens(text):
    """
    Jinja2 filter.
    Adds &shy; marks to suggest word breaks.
    Usage: {{ page.foo | hyphens }}.
    """
    dic = pyphen.Pyphen(lang=DEFAULT_LANG)
    wordlist = text.split()
    hyphenated = []
    for word in wordlist:
        if word in NON_BREAKABLE_WORDS:
            hyphenated.append(word)
        else:
            hyphenated.append(dic.inserted(word, "&shy;"))
    return " ".join(hyphenated)


def dont_break(text):
    """
    Jinja2 filter.
    Adds a non-breakable space after short prepositions.
    Usage: {{ page.foo | dont_break }}
    """
    shortwords = r"([aA]|[Aa]nd?|[Pr]ro|[Th]he|[Oo]f|[Tt]o|[Ii]t|[Ii]s|[Bb]y)\s"
    return re.sub(shortwords, r"\1&nbsp;", text)


I18N_UNTRANSLATED_PAGES = "remove"
I18N_UNTRANSLATED_ARTICLES = "remove"

DEFAULT_PAGINATION = 1

THEME = "product-template"

PIWIK_URL = "piwik.nic.cz"
PIWIK_SITE_ID = 41

# static paths will be copied under the same name
STATIC_PATHS = ["images", "documentation", "js", "css", "benchmark"]
PAGE_EXCLUDES = ["documentation"]
ARTICLE_EXCLUDES = ["documentation"]


def lookup_lang_name(lang_code):
    """
    Jinja2 filter.
    Returns full name of site language.
    """
    return languages_lookup[lang_code]


PLUGIN_PATHS = ["./pelican-plugins"]
ASSET_SOURCE_PATHS = ["static"]

PLUGINS = [
    "minify",
    "assets",
    "i18n_subsites"
]

JINJA_FILTERS = {
    "lookup_lang_name": lookup_lang_name,
    "dont_break": dont_break,
    "hyphens": hyphens
}

JINJA_ENVIRONMENT = {
    "extensions": ["jinja2.ext.i18n"]
}

FEED_ALL_RSS = "feeds/knot-resolver.xml"

SOCIAL = (("Twitter", "https://twitter.com/knotdns"),
          )

ARTICLE_URL = "{date:%Y}-{date:%m}-{date:%d}-{slug}.html"
ARTICLE_SAVE_AS = ARTICLE_URL

CATEGORY_SAVE_AS = ""
TAG_SAVE_AS = ""
AUTHOR_SAVE_AS = ""
ARCHIVES_SAVE_AS = ""
AUTHORS_SAVE_AS = ""
CATEGORIES_SAVE_AS = ""
TAGS_SAVE_AS = ""
PAGE_SAVE_AS = "{slug}/index.html"
PAGE_URL = "{slug}/"

RELATIVE_URLS = True

SUMMARY_MAX_LENGTH = 30

MINIFY = {
    "remove_comments": True,
    "remove_all_empty_space": False,
    "remove_optional_attribute_quotes": False
}
