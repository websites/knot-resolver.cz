:title: Home
:slug: index
:menuorder: 0
:intro_text: The Knot Resolver is a caching DNS resolver
             scalable from huge resolver farms down to home network routers.
:partners_title: Knot Resolver is trusted by companies worldwide


.. container:: index-partners

   Knot Resolver is trusted by companies worldwide

   .. container:: index-partners-logos

      .. raw:: html

         <img src="images/partners/cetin.svg" />
         <img src="images/partners/cloudflare.svg" />
         <img src="images/partners/whalebone.svg" />



Why Knot?
=========

Why not! ;-) Knot Resolver is a modern resolver implementation designed for
scalability, resiliency, and flexibility. Its design is different from other resolvers:
The core architecture is tiny and efficient, and most of the
`rich features <https://knot-resolver.readthedocs.io/en/stable/index.html>`__
are implemented as optional modules,
which limits attack surface and improves performance.

.. container:: cta

    `Get the Knot Resolver </download/>`_



.. container:: index-features
   
   .. container:: index-feature-box

      .. raw:: html

            <img src="images/features/open_source.svg" />

      Open source

      Knot Re­solver is open-­source. It is com­plete­ly free to down­load and use. The source code is avail­able un­der GPL li­cense. Our de­vel­op­ment process is trans­par­ent and driv­en by the needs of com­mu­ni­ty and do­nat­ing user­s.

   .. container:: index-feature-box

      .. raw:: html

            <img src="images/features/feature_packed.svg" />

      Feature-packed

      Bat­ter­ies in­cluded, but op­tional! Many re­solver fea­tures are avail­able out-of-the-box as mod­ules while keep­ing core tiny and ef­fi­cient. Mo­du­lar ar­chi­tec­ture pro­vides a state-ma­chine like API for ex­ten­sions, and the C and Lua mod­ules make it great to tap into the res­o­lu­tion process. A novel fil­ter­ing tech­niques are just cou­ple lines of code away. It's the OpenResty of DNS.

   .. container:: index-feature-box

      .. raw:: html

            <img src="images/features/high_performance.svg" />

      High performance

      The serv­er adopts a dif­fer­ent scal­ing strat­e­gy than the rest of the DNS re­cur­sors - no thread­ing, shared-noth­ing ar­chi­tec­ture (ex­cept cache that may be shared). You can start, stop, or re­config­ure in­stances with­out downtime.

   .. container:: index-feature-box

      .. raw:: html

            <img src="images/features/secure_and_stable.svg" />

      Secure and stable

      The code is be­ing con­stant­ly checked by an ex­ten­sive test­ing suite to at­tain sta­bil­i­ty, as­sure in­ter­op­er­abil­i­ty with oth­er DNS im­ple­men­ta­tion­s, avoid per­for­mance re­gres­sion­s, and cir­cum­vent pos­si­ble se­cu­ri­ty-re­lat­ed prob­lem­s.



People said
===========

What our customers love most about Knot Resolver

.. container:: index-boxes index-testimonials

   .. container:: index-boxes-content

      .. container:: index-box index-testimonial

         .. raw:: html

            <img src="images/partners/cetin.svg" />

         Knot Resolver has been a part of our infrastructure since 2017. We decided to use Knot Resolver because we already had a good experience with other CZ.NIC projects (BIRD, FRED, ...), and we are confident of their ability to solve problems quickly. 
         
         Milan Sýkora 
         
         Network Services Specialist, CETIN
      
      .. container:: index-box index-testimonial

         .. raw:: html

            <img src="images/partners/cloudflare.svg" />

         Knot Resolver is the cornerstone of our public resolver service running on 1.1.1.1. It provides the standard functionality we need and its modular architecture allowed us to develop additional features unique to our platform.

         Ólafur Guðmundsson 
         
         Engineering director, Cloudflare

      .. container:: index-box index-testimonial

         .. raw:: html

            <img src="images/partners/whalebone.svg" />

         After evaluation of a variety of DNS resolvers, we found that Knot Resolver provides us with the best platform for building our unique threat prevention and DNS filtering solution.
         
         Robert Šefr 

         CTO, Whalebone


.. `More reviews </download/>`_



.. container:: index-boxes

   .. container:: index-boxes-content
   
      .. container:: index-box index-talksupport

         Community

         Join our community and ask any questions, post sugesstions or comments.

         .. container:: cta

            `Join mailing list or chat </support/free/>`_

      .. container:: index-box index-talksupport

         Support

         Knot Resolver is free software and is available free of charge. Users who depend on our software also appreciate professional support with guaranteed SLA.

         .. container:: cta

            `Get professional support </support/pro/>`_



One more thing
==============

Knot Resolver is not the only child of the Knot family. Our other project Knot DNS is a high-performance authoritative-only DNS server which supports all key features of the modern domain name system.

`Learn more <https://www.knot-dns.cz/>`_
