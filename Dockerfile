FROM debian:11-slim

RUN \
	apt-get update -yqq && \
	apt-get install -yqq python3-pip ca-certificates git locales-all

RUN ln -s '/usr/bin/python3.9' '/usr/bin/python'

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
