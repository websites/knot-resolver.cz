:title: Knot Resolver 5.5.3 released
:slug: knot-resolver-5.5.3
:date: 2022-09-21 14:30
:lang: en

Security
--------
- fix CPU-expensive DoS by malicious domains - CVE-2022-40188

Improvements
------------
- fix config_tests on macOS (both HW variants)