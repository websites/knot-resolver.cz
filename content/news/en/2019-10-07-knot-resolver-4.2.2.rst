:title: Knot Resolver 4.2.2 released
:slug: knot-resolver-4.2.2
:date: 2019-10-07 14:30
:lang: en

Bugfixes
--------
- lua bindings: fix a 4.2.1 regression on 32-bit systems (#514)
  which also fixes libknot 2.9 support on all systems
