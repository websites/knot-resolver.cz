:title: Knot Resolver 5.7.0 released
:slug: knot-resolver-5.7.0
:date: 2023-08-22 14:30
:lang: en

Security
--------
- avoid excessive TCP reconnections in a few more cases
  Like before, the remote server had to behave nonsensically in order
  to inflict this upon itself, but it might be abusable for DoS.

  We thank Ivan Jedek from OryxLabs for reporting this.

Improvements
------------
- forwarding mode: tweak dealing with failures from forwarders,
  in particular prefer sending CD=0 upstream (!1392)

Bugfixes
--------
- fix unusual timestamp format in debug dumps of records (!1386)
- adjust linker options; it should help less common platforms (!1384)
- hints module: fix names inside home.arpa. (!1406)
- EDNS padding (RFC 8467) compatibility with knot-dns 3.3 libs (!1422)
