:title: Knot Resolver 1.5.3 released
:slug: knot-resolver-1.5.3
:date: 2018-01-23 15:00
:lang: en

Knot Resolver 1.5.3 is a tiny bugfix release.

Bugfixes
========
- fix the hints module on some systems, e.g. Fedora.
  Symptom: `undefined symbol: engine_hint_root_file`

