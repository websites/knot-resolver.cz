:title: Knot Resolver 2.4.1 released
:slug: knot-resolver-2.4.1
:date: 2018-08-02 14:00
:lang: en

Security
--------
- fix CVE-2018-10920: Improper input validation bug in DNS resolver component
  (security!7, security!9)

Bugfixes
--------
- cache: fix TTL overflow in packet due to min_ttl (#388, security!8)
- TLS session resumption: avoid bad scheduling of rotation (#385)
- HTTP module: fix a regression in 2.4.0 which broke custom certs (!632)
- cache: NSEC3 negative cache even without NS record (#384)
  This fixes lower hit rate in NSEC3 zones (since 2.4.0).
- minor TCP and TLS fixes (!623, !624, !626)
