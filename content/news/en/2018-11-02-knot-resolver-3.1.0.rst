:title: Knot Resolver 3.1.0 released
:slug: knot-resolver-3.1.0
:date: 2018-11-02 16:00
:lang: en

Incompatible changes
--------------------
- hints.use_nodata(true) by default; that's what most users want
- libknot >= 2.7.2 is required

Improvements
------------
- cache: handle out-of-space SIGBUS slightly better (#197)
- daemon: improve TCP timeout handling (!686)

Bugfixes
--------
- cache.clear('name'): fix some edge cases in API (#401)
- fix error handling from TLS writes (!669)
- avoid SERVFAILs due to certain kind of NS dependency cycles (#374)
