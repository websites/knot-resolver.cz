:title: Knot Resolver 5.6.0 released
:slug: knot-resolver-5.6.0
:date: 2023-01-26 18:30
:lang: en

Security
--------
- avoid excessive TCP reconnections in some cases (!1380)
  For example, a DNS server that just closes connections without answer
  could cause lots of work for the resolver (and itself, too).
  The number of connections could be up to around 100 per client's query.

  We thank Xiang Li from NISL Lab, Tsinghua University,
  and Xuesong Bai and Qifan Zhang from DSP Lab, UCI.

Improvements
------------
- daemon: feed server selection with more kinds of bad-answer events (!1380)
- cache.max_ttl(): lower the default from six days to one day
  and apply both limits to the first uncached answer already (!1323 #127)
- depend on jemalloc, preferably, to improve memory usage (!1353)
- no longer accept DNS messages with trailing data (!1365)
- policy.STUB: avoid applying aggressive DNSSEC denial proofs (!1364)
- policy.STUB: avoid copying +dnssec flag from client to upstream (!1364)

Bugfixes
--------
- policy.DEBUG_IF: don't print client's packet unconditionally (!1366)
