:title: Knot Resolver 1.5.2 released
:slug: knot-resolver-1.5.2
:date: 2018-01-22 13:00
:lang: en

Knot Resolver 1.5.2 is a small security release.

Security
========
- fix CVE-2018-1000002: insufficient DNSSEC validation, allowing
  attackers to deny existence of some data by forging packets.
  Some combinations pointed out in RFC 6840 sections 4.1 and 4.3
  were not taken into account.

Bugfixes
========
- memcached: fix fallout from module rename in 1.5.1

