:title: Free support
:slug: support/free
:lang: en
:menuorder: 0

Free support for the **latest version** of Knot Resolver is available via English-speaking public fora:

- `mailing list`_ - suitable for questions
- `issue tracker`_ - suitable for suspected bugs or feature requests
- `Gitter channel`_ - suitable for interactive debugging

This support is provided by Knot Resolver community, i.e. users and also Knot Resolver developers who respond when they have time. Please keep in mind free support channels do not offer any guarantees about response time or correctness. It offers *free support for a free product*.

If there is a bug in Knot Resolver, something is unclear in documentation, or a feature behaves in surpsising ways, we, the Knot Resolver developers, will happily fix it for you!

This free support has a single condition: Communication in public, on our `mailing list`_, `issue tracker`_, or `Gitter channel`_.

Out in the open
===============

- If you want to be part of the Knot Resolver community and get free support, everything we need to know to help you needs to be communicated publicly.

By providing support in the open, other people can learn, search engines can pick up answers, the community can pitch in with solutions or suggestions. Doing free support this way provides a true public benefit.

If you have a domain that does not resolve, we need the actual name of that domain, not *example.com*. If we cannot query your nameservers because you will not tell us their IP address, we cannot help you. If you wrote a custom script that does not do what you want, but you cannot post that script to the `mailing list`_ because it is proprietary, we cannot help you.

If you cannot share sufficient detail of your issue, you will need to reproduce the problem using data you can share. Once you publish steps to reproduce the problem we will gladly help you with your problem.

(This support policy is about the things *we need to know to solve your issue*, so obviously we will not ask for your IP address if it is not relevant to your issue.)


Private support options
=======================
If you cannot share sufficient details,
or do not want to spend time on creating minimal reproducer,
consider `becoming a customer </support/pro/>`_ to get access to private support.
It takes money to develop quality products, and paying customers are those who allow Knot Resolver to live and thrive.

If you have contributed to Knot Resolver development in some way, or work on a project we depend on, we might make an exception and help you more privately.

Finally, Knot Resolver has very similar free support policy as PowerDNS and this page is inspired by
`PowerDNS blog post <https://blog.powerdns.com/2016/01/18/open-source-support-out-in-the-open/>`_.

.. _mailing list: https://lists.nic.cz/postorius/lists/knot-resolver-users.lists.nic.cz/
.. _issue tracker: https://gitlab.nic.cz/knot/knot-resolver/issues
.. _Gitter channel: https://gitter.im/CZ-NIC/knot-resolver
