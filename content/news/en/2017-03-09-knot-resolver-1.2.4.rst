:title: Knot Resolver 1.2.4 released
:slug: knot-resolver-1.2.4
:date: 2017-03-07 14:30
:lang: en

Knot Resolver 1.2.4 has been released.

Security
========

-  Knot Resolver 1.2.0 and higher could return AD flag for insecure
   answer if the daemon received answer with invalid RRSIG several times
   in a row.

Improvements
============

-  modules/policy: allow QTRACE policy to be chained with other policies
-  hints.add\_hosts(path): a new property
-  module: document the API and simplify the code
-  policy.MIRROR: support IPv6 link-local addresses
-  policy.FORWARD: support IPv6 link-local addresses
-  add net.outgoing\_{v4,v6} to allow specifying address to use for
   connections

Bugfixes
========

-  layer/iterate: some improvements in cname chain unrolling
-  layer/validate: fix duplicate records in AUTHORITY section in case of
   WC expansion proof
-  lua: do *not* truncate cache size to unsigned
-  forwarding mode: correctly forward +cd flag
-  fix a potential memory leak
-  don't treat answers that contain DS non-existence proof as insecure
-  don't store NSEC3 and their signatures in the cache
-  layer/iterate: when processing delegations, check if qname is at or
   below new authority
