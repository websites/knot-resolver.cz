:title: Knot Resolver 5.4.2 released
:slug: knot-resolver-5.4.2
:date: 2021-10-13 14:00
:lang: en

Improvements
------------
- dns64 module: also map the reverse (PTR) subtree (#478, !1201)
- dns64 module: allow disabling based on client address (#368, !1201)
- dns64 module: allow configuring AAAA subnets not allowed in answer (!1201)
- nameserver selection algorithm: improve IPv6 avoidance if broken (!1207)

Bugfixes
--------
- lua: log() output is visible with default log level again (!1208)
- build: fix when knot-dns headers are on non-standard location (!1210)
