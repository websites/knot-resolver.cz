:title: Development
:slug: development
:menu: Development
:lang: en
:menuorder: 3

Repository
==========

-  https://gitlab.nic.cz/knot/knot-resolver.git
-  See `Contributing <https://gitlab.nic.cz/knot/knot-resolver/-/blob/master/CONTRIBUTING.md>`__

Gitlab
======

-  `Browse
   source <https://gitlab.nic.cz/knot/knot-resolver/tree/master>`__
-  `Issues or bug
   reports <https://gitlab.nic.cz/knot/knot-resolver/issues>`__

Community
=========

-  Join the chat at `Gitter`_
-  E-mail conference: `knot-resolver-users@lists.nic.cz`_
-  Announce-only mailing list: `knot-resolver-announce@lists.nic.cz`_
-  You can also report bugs to: `knot-resolver@labs.nic.cz`_

.. _Gitter: https://gitter.im/CZ-NIC/knot-resolver
.. _knot-resolver-users@lists.nic.cz:    https://lists.nic.cz/postorius/lists/knot-resolver-users.lists.nic.cz/
.. _knot-resolver-announce@lists.nic.cz: https://lists.nic.cz/postorius/lists/knot-resolver-announce.lists.nic.cz/
.. _knot-resolver@labs.nic.cz: mailto:knot-resolver@labs.nic.cz

Security issues
===============

Please `contact us </contact/>`_.
