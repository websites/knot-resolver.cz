:title: Knot Resolver 5.4.4 released
:slug: knot-resolver-5.4.4
:date: 2022-01-05 14:30
:lang: en

Bugfixes
--------
- fix bad zone cut update in certain cases (e.g. AWS; !1237)
