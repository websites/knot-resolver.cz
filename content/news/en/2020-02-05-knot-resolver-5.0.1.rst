:title: Knot Resolver 5.0.1 released
:slug: knot-resolver-5.0.1
:date: 2020-02-05 16:30
:lang: en

Bugfixes
--------
- systemd: use correct cache location for garbage collector (#543)

Improvements
------------
- cache: add cache.fssize() lua function to configure entire free disk space on
  dedicated cache partition (#524, !932)
