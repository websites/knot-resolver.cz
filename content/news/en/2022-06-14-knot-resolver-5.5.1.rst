:title: Knot Resolver 5.5.1 released
:slug: knot-resolver-5.5.1
:date: 2022-06-14 14:30
:lang: en

Improvements
------------
- daemon/tls: disable TLS resumption via tickets for TLS <= 1.2 (#742, !1295)
- daemon/http: DoH now responds with proper HTTP codes (#728, !1279)
- renumber module: allow rewriting subnet to a single IP (!1302)
- renumber module: allow arbitrary netmask (!1306)
- nameserver selection algorithm: improve IPv6 avoidance if broken (!1298)

Bugfixes
--------
- modules/dns64: fix incorrect packet writes for cached packets (#727, !1275)
- xdp: make it work also with libknot 3.1 (#735, !1276)
- prefill module: fix lockup when starting multiple idle instances (!1285)
- validator: fix some failing negative NSEC proofs (!1294, #738, #443)
