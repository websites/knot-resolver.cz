:title: Knot Resolver 1.2.0 released
:slug: knot-resolver-1.2.0
:date: 2017-01-25 14:00
:lang: en

Knot Resolver 1.2.0 has been released.

CZ.NIC is proud to release a new release of Knot Resolver.

The Knot Resolver team has worked very hard to bring:

-  reworked DNSSEC Validation, that fixes several know problems with
   less standard DNS configurations, and it is also a solid base for
   further improvements
-  optional EDNS(0) Padding support for DNS over TLS
-  support for debugging DNSSEC with CD bit
-  DNS over TLS is now able to create ephemeral certs on the runtime
   (Thanks Daniel Kahn Gilmore for contributing to DNS over TLS
   implementation in Knot Resolver.)
-  configurable minimum and maximum TTL (default 6 days)
-  configurable pseudo-random reordering of RR sets
-  new module 'version' that can call home and report new versions and
   security vulnerabilities to the log file

This release also fixes bugs, most notable ones:

-  The resolver was setting AD flag when running in a forwarding mode.
   Thanks Stéphane Bortzmeyer for reporting this issue!
-  We now correctly return RCODE=NOTIMPL on meta-queries and non IN
   class queries
-  Fix crash in hints module when hints file was empty
-  Fix non-lowercase hints

We also have a new LRU implementation under the hood.

That's it! Thank you for using Knot Resolver. And if you are not using
it yet, please give it a try.
