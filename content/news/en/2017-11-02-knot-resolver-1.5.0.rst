:title: Knot Resolver 1.5.0 released
:slug: knot-resolver-1.5.0
:date: 2017-11-02 14:00
:lang: en

Knot Resolver 1.5.0 has been released.

Bugfixes
========

-  fix loading modules on Darwin

Improvements
============

-  new module ta\_signal\_query supporting Signaling Trust Anchor
   Knowledge using Keytag Query (RFC 8145 section 5); it is enabled by
   default
-  attempt validation for more records but require it for fewer of them
   (e.g. avoids SERVFAIL when server adds extra records but omits
   RRSIGs)
