:title: Knot Resolver 5.7.4 released
:slug: knot-resolver-5.7.4
:date: 2024-07-23 14:45
:lang: en

Security
--------
- reduce buffering of transmitted data, especially TCP-based in userspace
  Also expose some of the new tweaks in lua:

  - (require 'ffi').C.the_worker.engine.net.tcp.user_timeout = 1000
  - (require 'ffi').C.the_worker.engine.net.listen_{tcp,udp}_buflens.{snd,rcv}

Improvements
------------
- add the fresh DNSSEC root key "KSK-2024" already, Key ID 38696 (!1556)

Incompatible changes
--------------------
- libknot 3.0.x support is dropped (!1558)
  Upstream last maintained 3.0.x in spring 2022.
