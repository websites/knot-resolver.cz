:title: Survey among DNS resolver administrators
:slug: survey
:lang: en
:menuorder: 0

In 2020 we ran a survey targeting administrators of **resolver software from any vendor**.
You can read about the results on our blog:
https://en.blog.nic.cz/2021/11/09/survey-results-dns-resolvers-configuration/

