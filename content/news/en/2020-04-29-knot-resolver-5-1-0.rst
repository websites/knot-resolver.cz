:title: Knot Resolver 5.1.0 released
:slug: knot-resolver-5.1.0
:date: 2020-04-29 13:30
:lang: en

Improvements
------------
- cache garbage collector: reduce filesystem operations when idle (!946)
- policy.DEBUG_ALWAYS and policy.DEBUG_IF for limited verbose logging (!957)
- daemon: improve TCP query latency under heavy TCP load (!968)
- add policy.ANSWER action (!964, #192)
- policy.rpz support fake A/AAAA (!964, #194)

Bugfixes
--------
- cache: missing filesystem support for pre-allocation is no longer fatal (#549)
- lua: policy.rpz() no longer watches the file when watch is set to false (!954)
- fix a strict aliasing problem that might've lead to "miscompilation" (!962)
- fix handling of DNAMEs, especially signed ones (#234, !965)
- lua resolve(): correctly include EDNS0 in the virtual packet (!963)
  Custom modules might have been confused by that.
- do not leak bogus data into SERVFAIL answers (#396)
- improve random Lua number generator initialization (!979)
- cache: fix CNAME caching when validation is disabled (#472, !974)
- cache: fix CNAME caching in policy.STUB mode (!974)
- prefill: fix crash caused by race condition with resolver startup (!983)
- webmgmt: use javascript scheme detection for websockets' protocol (#546)
- daf module: fix del(), deny(), drop(), tc(), pass() functions (#553, !966)
- policy and daf modules: expose initial query when evaluating postrules (#556)
- cache: fix some cases of caching answers over 4 KiB (!976)
- docs: support sphinx 3.0.0+ (!978)

Incompatible changes
--------------------
- minor changes in module API; see upgrading guide:
  https://knot-resolver.readthedocs.io/en/stable/upgrading.html
