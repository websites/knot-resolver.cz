:title: Knot Resolver 2.4.0 released
:slug: knot-resolver-2.4.0
:date: 2018-07-03 12:00
:lang: en

Incompatible changes
--------------------
- minimal libknot version is now 2.6.7 to pull in latest fixes (#366)

Security
--------
- fix a rare case of zones incorrectly downgraded to insecure status (!576)

New features
------------
- TLS session resumption (RFC 5077), both server and client (!585, #105)
  (disabled when compiling with gnutls < 3.5)
- TLS_FORWARD policy uses system CA certificate store by default (!568)
- aggressive caching for NSEC3 zones (!600)
- optional protection from DNS Rebinding attack (module rebinding, !608)
- module bogus_log to log DNSSEC bogus queries without verbose logging (!613)

Bugfixes
--------
- prefill: fix ability to read certificate bundle (!578)
- avoid turning off qname minimization in some cases, e.g. co.uk. (#339)
- fix validation of explicit wildcard queries (#274)
- dns64 module: more properties from the RFC implemented (incl. bug #375)

Improvements
------------
- systemd: multiple enabled kresd instances can now be started using kresd.target
- ta_sentinel: switch to version 14 of the RFC draft (!596)
- support for glibc systems with a non-Linux kernel (!588)
- support per-request variables for Lua modules (!533)
- support custom HTTP endpoints for Lua modules (!527)
