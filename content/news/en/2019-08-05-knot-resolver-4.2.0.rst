:title: Knot Resolver 4.2.0 released
:slug: knot-resolver-4.2.0
:date: 2019-08-05 17:00
:lang: en

Improvements
------------
- queries without RD bit set are REFUSED by default (!838)
- support forwarding to multiple targets (!825)

Bugfixes
--------
- tls_client: fix issue with TLS session resumption (#489)
- rebinding module: fix another false-positive assertion case (!851)

Module API changes
------------------
- kr_request::add_selected is now really put into answer,
  instead of the "duplicate" ::additional field (#490)
