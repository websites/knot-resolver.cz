:title: Support
:slug: support
:lang: en
:menu: Support
:menuorder: 4

Users of Knot Resolver have two main support options: `Free </support/free/>`__ and `Professional </support/pro/>`__.

- `Free support </support/free/>`__ via public channels.
   Suitable for users who just want to try out
   Knot Resolver or users who's operations do not depend on DNS resolving.

- `Professional support </support/pro/>`__ services with SLA and NDA.
   Suitable for users who consider DNS resolving critial for their bussiness,
   or simply want to support further development of Knot Resolver open-source
   project.

.. raw:: html

    <a class="donation-button" href="https://donations.nic.cz/en/donate/?project=knot-resolver">Donate</a>