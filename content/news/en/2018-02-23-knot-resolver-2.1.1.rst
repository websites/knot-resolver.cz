:title: Knot Resolver 2.1.1 released
:slug: knot-resolver-2.1.1
:date: 2018-02-23 14:00
:lang: en

Bugfixes
--------
- when iterating, avoid unnecessary queries for NS in insecure parent.
  This problem worsened in 2.0.0. (#246)
- prevent UDP packet leaks when using TLS forwarding
- fix the hints module also on some other systems, e.g. Gentoo.

