:title: Knot Resolver 5.1.1 released
:slug: knot-resolver-5.1.1
:date: 2020-05-19 11:00
:lang: en

Security
--------
- fix CVE-2020-12667: mitigation for NXNSAttack_ DNS protocol vulnerability

Bugfixes
--------
- control sockets: recognize newline as command boundary

For more information please see blog post about NXNSAttack_.

.. _NXNSAttack: https://en.blog.nic.cz/2020/05/19/nxnsattack-upgrade-resolvers-to-stop-new-kind-of-random-subdomain-attack/
