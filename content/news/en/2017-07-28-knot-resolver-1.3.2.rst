:title: Knot Resolver 1.3.2 released
:slug: knot-resolver-1.3.2
:date: 2017-06-28 14:00
:lang: en

Knot Resolver 1.3.2 has been released.

Security
========

-  fix possible opportunities to use insecure data from cache as keys
   for validation

Bugfixes
========

-  daemon: check existence of config file even if rundir isn't specified
-  policy.FORWARD and STUB: use RTT tracking to choose servers (#125,
   #208)
-  dns64: fix CNAME problems (#203) It still won't work with
   policy.STUB.
-  hints: better interpretation of hosts-like files (#204) also, error
   out if a bad entry is encountered in the file
-  dnssec: handle unknown DNSKEY/DS algorithms (#210)
-  predict: fix the module, broken since 1.2.0 (#154)

Improvements
============

-  embedded LMDB fallback: update 0.9.18 -> 0.9.21
