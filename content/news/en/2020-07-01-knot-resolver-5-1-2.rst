:title: Knot Resolver 5.1.2 released
:slug: knot-resolver-5.1.2
:date: 2020-07-01 14:30
:lang: en

Bugfixes
--------
- hints module: NODATA answers also for non-address queries (!1005)
- tls: send alert to peer if handshake fails (!1007)
- cache: fix interaction between LMDB locks and preallocation (!1013)
- cache garbage collector: fix flushing of messages to logs (!1009)
- cache garbage collector: fix insufficient GC on 32-bit systems (!1009)
- graphite module: do not block resolver on TCP failures (!1014)
- policy.rpz various fixes (!1016): $ORIGIN issues,
  precision of warnings, allow answering with multi-RR sets
