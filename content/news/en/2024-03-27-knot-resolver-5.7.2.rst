:title: Knot Resolver 5.7.2 released
:slug: knot-resolver-5.7.2
:date: 2024-03-27 14:30
:lang: en

Bugfixes
--------
- fix on 32-bit systems with 64-bit time_t (!1510)
