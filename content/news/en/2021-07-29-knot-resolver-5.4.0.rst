:title: Knot Resolver 5.4.0 released
:slug: knot-resolver-5.4.0
:date: 2021-07-29 16:00
:lang: en

Improvements
------------
- fine grained logging and syslog support (!1181)
- expose HTTP headers for processing DoH requests (!1165)
- improve assertion mechanism for debugging (!1146)
- support apkg tool for packaging workflow (!1178)
- support Knot DNS 3.1 (!1192, !1194)

Bugfixes
--------
- trust_anchors.set_insecure: improve precision (#673, !1177)
- plug memory leaks related to TCP (!1182)
- policy.FLAGS: fix not applying properly in edge cases (!1179)
- fix a crash with older libuv inside timer processing (!1195)

Incompatible changes
--------------------
- see upgrading guide:
  https://knot-resolver.readthedocs.io/en/stable/upgrading.html#to-5-4
- legacy DoH implementation configuration in net.listen() was renamed from
  kind="doh" to kind="doh_legacy" (!1180)
