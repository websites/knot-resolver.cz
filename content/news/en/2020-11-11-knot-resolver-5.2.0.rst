:title: Knot Resolver 5.2.0 released
:slug: knot-resolver-5.2.0
:date: 2020-11-11 14:00
:lang: en

Improvements
------------
- doh2: add native C module for DNS-over-HTTPS (#600, !997)
- xdp: add server-side XDP support for higher UDP performance (#533, !1083)
- lower default EDNS buffer size to 1232 bytes (#538, #300, !920);
  see https://dnsflagday.net/2020/
- net: split the EDNS buffer size into upstream and downstream (!1026)
- lua-http doh: answer to /dns-query endpoint as well as /doh (!1069)
- improve resiliency against UDP fragmentation attacks (disable PMTUD) (!1061)
- ta_update: warn if there are differences between statically configured
  keys and upstream (#251, !1051)
- human readable output in interactive mode was improved
- doc: generate info page (!1079)
- packaging: improve sysusers and tmpfiles support (!1080)

Bugfixes
--------
- avoid an assert() error in stash_rrset() (!1072)
- fix emergency cache locking bug introduced in 5.1.3 (!1078)
- migrate map() command to control sockets; fix systemd integration (!1000)
- fix crash when sending back errors over control socket (!1000)
- fix SERVFAIL while processing forwarded CNAME to a sibling zone (#614, !1070)

Incompatible changes
--------------------
- see upgrading guide:
  https://knot-resolver.readthedocs.io/en/stable/upgrading.html#to-5-2
- minor changes in module API
- control socket API commands have to be terminated by ``\n``
- graphite: default prefix now contains instance identifier (!1000)
- build: meson >= 0.49 is required (!1082)
