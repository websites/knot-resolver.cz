:title: Knot Resolver 4.2.1 released
:slug: knot-resolver-4.2.1
:date: 2019-09-26 14:30
:lang: en

Bugfixes
--------
- rebinding module: fix handling some requests, respect ALLOW_LOCAL flag
- fix incorrect SERVFAIL on cached bogus answer for +cd request (!860)
  (regression since 4.1.0 release, in less common cases)
- prefill module: allow a different module-loading style (#506)
- validation: trim TTLs by RRSIG's expiration and original TTL (#319, #504)
- NS choice algorithm: fix a regression since 4.0.0 (#497, !868)
- policy: special domains home.arpa. and local. get NXDOMAIN (!855)

Improvements
------------
- add compatibility with (future) libknot 2.9
